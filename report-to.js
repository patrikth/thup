const express = require('express');
const app = express();
const port = 8000;
app.use(express.json({
  type: ['application/json', 'application/csp-report', 'application/reports+json']
}));
app.use(express.urlencoded());

app.get('/', (request, response) => {
  // Note:  report-to replaces report-uri, but it is not supported yet.
  response.set('Content-Security-Policy-Report-Only',
      `default-src 'self'; report-to csp-endpoint`);
   // Note: report_to and not report-to for NEL.
  response.set('NEL', `{"report_to": "network-errors", "max_age": 2592000}`);

  // The Report-To header tells the browser where to send
  // CSP violations, browser interventions, deprecations, and network errors.
  // The default group (first example below) captures interventions and
  // deprecation reports. Other groups are referenced by their "group" name.
  response.set('Report-To', `{"max_age": 2592000, "endpoints": [{ "url": "http://localhost:8000/" }], }, {"group": "csp-endpoint","max_age": 2592000,"endpoints": [{"url": "http://localhost:8000/csp-reports"}],}, {"group": "network-errors","max_age": 2592000,"endpoints": [{"url": "http://localhost:8000/network-reports"}]}`);
  response.sendFile('index.html' , { root : __dirname});
});

// This function did not work and is not used - THUP
function echoReports(request, response) {
  // Record report in server logs or otherwise process results.
  for (const report of request.body) {
    console.log(report.body);
  }
  response.send(request.body);
}

app.post('/csp-reports', (request, response) => {
  console.log(`CSP violation reports:`);
  console.log(request.body);
  response.status(204).send();
  //echoReports(request, response);
});

app.post('/network-reports', (request, response) => {
  console.log(`Network error reports:`);
  console.log(request.body);
  response.status(204).send();
  //echoReports(request, response);
});

app.post('/reports', (request, response) => {
  console.log(`deprecation/intervention reports:`);
  console.log(request.body);
  response.status(204).send();
  echoReports(request, response);
});

const listener = app.listen(port, () => {
  console.log(`Your app is listening on port ${port}`);
});
